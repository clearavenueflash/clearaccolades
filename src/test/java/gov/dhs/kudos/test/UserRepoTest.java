/*
 *
 */
package gov.dhs.kudos.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.State;
import gov.dhs.kudos.repository.RoleRepository;
import gov.dhs.kudos.repository.UserRepository;
import gov.dhs.kudos.test.config.TestJpaConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJpaConfig.class })
public class UserRepoTest {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RoleRepository roleRepo;

	Role adminRole;
	Role userRole;
	Role managerRole;

	final Employee employee1 = new Employee("Bill", "Hunt", "password1", "bill.hunt@clearavenue.com", State.APPROVED);
	final Employee employee2 = new Employee("John", "Doe", "password1", "john.doe@clearavenue.com", State.APPROVED);
	final Employee manager1 = new Employee("Chan", "Smith", "password1", "chandler.smith@clearavenue.com", State.APPROVED);
	final Employee manager2 = new Employee("Shane", "Hogan", "password1", "shane.hogan@clearavenue.com", State.APPROVED);

	@Before
	public void loadDB() {
		adminRole = roleRepo.save(new Role(RoleType.ADMIN));
		userRole = roleRepo.save(new Role(RoleType.EMPLOYEE));

		employee1.setRole(adminRole);
		employee2.setRole(userRole);
		manager1.setRole(managerRole);
		manager2.setRole(managerRole);

		userRepo.save(Arrays.asList(employee1, employee2, manager1, manager2));
	}

	@After
	public void cleanDB() {
		userRepo.deleteAll();
		roleRepo.deleteAll();
	}

	@Test
	public void countAllUsers() {
		final long userCount = userRepo.count();
		assertEquals(4, userCount);
	}

	@Test
	public void checkModifyPassword() {

		Employee employee = userRepo.findByEmail(employee1.getEmail());
		employee.setLastName("Modified");
		assertTrue(passwordEncoder.matches("password1", employee.getPassword()));
		userRepo.save(employee);

		employee = userRepo.findByEmail(employee1.getEmail());
		assertEquals("Modified", employee.getLastName());
		assertTrue(passwordEncoder.matches("password1", employee.getPassword()));
	}

}
