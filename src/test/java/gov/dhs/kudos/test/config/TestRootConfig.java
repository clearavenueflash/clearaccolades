package gov.dhs.kudos.test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "gov.dhs.kudos.test.config", "gov.dhs.kudos.security" })
public class TestRootConfig {

}
