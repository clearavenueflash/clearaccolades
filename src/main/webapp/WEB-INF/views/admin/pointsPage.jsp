<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url value="/resources" var="resources" />
<c:url value="/admin" var="admin" />
<c:url value="/logout" var="logout" />
<c:url value="/admin/employeeListing" var="empListin" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Points Page</title>

    <!--[if lt IE 9]>
    <script src="${resources}/js/html5shiv.min.js"></script>
    <![endif]-->

    <link href="${resources}/css/uswds.min.css" rel="stylesheet" />
    <link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>
<body>
<a class="skipnav" href="#main-content">Skip to main content</a>
<header class="usa-site-header" role="banner">
    <div class="site-navbar">
        <a class="menu-btn" href="#">Menu</a>
        <div class="site-logo" id="logo">
            <em>MVP Kudos</em>
        </div>
        <ul class="usa-button-list usa-unstyled-list">
            <li><a id="points-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${empListin}'"> Employee Listing Page </a></li>
            <li><a id="leader-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> Leader Board </a></li>
            <li><a id="profile-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> My Profile </a></li>
            <li><a id="faq-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> FAQ </a></li>
            <li><a id="logout" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
        </ul>
    </div>
</header>

<div class="main-content usa-center" id="main-content">
    <div class="kudos-content usa-content">
        <button class="usa-button usa-button-primary">Export</button>
        <table>
            <thead>
            <tr>
                <th id="lastName" scope="col" aria-sort="ascending"><a href="">Nominee&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="firstName" scope="col"><a href="">Nominator&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="middle" scope="col"><a href="">M&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="comment" scope="col"><a href="">Comment&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="date" scope="col"><a href="">Date&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th scope="col">Action</th>
            </tr>
            </thead>

            <c:forEach var="k" items="${allKudos}">
                <tr>
                    <td>${k.byEmployee}</td>
                    <td>${k.forEmployee}</td>
                    <td>${k.middleInitial}</td>
                    <td>${k.comment}</td>
                    <td><fmt:formatDate type="date" value="${k.sent}"/></td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script src="${resources}/js/uswds.min.js"></script>
<script src="${resources}/js/jquery-3.1.0.min.js"></script>

<script>

</script>

</body>
</html>
