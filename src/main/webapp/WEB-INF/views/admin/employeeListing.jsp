<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/admin" var="admin" />
<c:url value="/logout" var="logout" />
<c:url value="/admin/pointsPage" var="points" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Listing Page</title>

    <!--[if lt IE 9]>
    <script src="${resources}/js/html5shiv.min.js"></script>
    <![endif]-->

    <link href="${resources}/css/uswds.min.css" rel="stylesheet" />
    <link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>
<body>
<a class="skipnav" href="#main-content">Skip to main content</a>
<header class="usa-site-header" role="banner">
    <div class="site-navbar">
        <a class="menu-btn" href="#">Menu</a>
        <div class="site-logo" id="logo">
            <em>MVP Kudos</em>
        </div>
        <ul class="usa-button-list usa-unstyled-list">
            <li><a id="points-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${points}'"> Points Page </a></li>
            <li><a id="leader-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> Leader Board </a></li>
            <li><a id="profile-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> My Profile </a></li>
            <li><a id="faq-button" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '#'"> FAQ </a></li>
            <li><a id="logout" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
        </ul>
    </div>
</header>

<div class="main-content usa-center" id="main-content">
    <div class="kudos-content usa-content">
    	<a class="usa-button usa-button-primary" href="/admin/exportEmployeeListing">Export</a>
        <table>
            <thead>
            <tr>
                <th id="lastName" scope="col" aria-sort="ascending"><a href="">Last Name&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="firstName" scope="col"><a href="">First Name&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="middle" scope="col"><a href="">Middle Initial&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th id="active" scope="col"><a href="">Active&#xA0;<span title="Sort">&#9650;</span></a></th>
                <th scope="col">Action</th>
            </tr>
            </thead>

            <c:forEach var="u" items="${allUsers}">
                <tr>
                    <td>${u.lastName}</td>
                    <td>${u.firstName}</td>
                    <td>${u.middleInitial}</td>
                    <td>${u.state}</td>
                    <td><spring:url value="/admin/modifyUser/${u.id}" var="userUrl" />
                        <button onclick="location.href='${userUrl}'">Update</button>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script src="${resources}/js/uswds.min.js"></script>
<script src="${resources}/js/jquery-3.1.0.min.js"></script>

<script>

</script>

</body>
</html>
