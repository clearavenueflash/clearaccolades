<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/logout" var="logout" />
<c:url value="/admin" var="admin" />
<c:url value="/employee/processKudos" var="processKudos" />
<c:url value="/kudos/giveKudos" var="giveKudos" />
<c:url value="/admin/reports" var="reports" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kudos</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
	<img src="${resources}/img/logo.svg" width="40%" />
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Employee Profile for: ${employee}</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a id="logout" class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>

	<div class="main-content" id="main-content">
		<div class="usa-content">
			<div class="usa-grid">
				<div class="usa-width-three-fourths">
					<p>Current count of my kudos: ${myKudos.size()}</p>
					<label>Send Kudos</label>
					<div class="panel show">
						<form:form class="usa-form" method="post" action="${processKudos}" modelAttribute="kudosForm">
							<fieldset>
								<legend>Kudos</legend>
								<label for="forEmployee">For User <span class="usa-additional_text">Required</span></label> <select name="forEmployee" id="forEmployee">
									<c:forEach var="u" items="${allusers}">
										<option value="${u.id}"><c:out value="${u.firstName} ${u.lastName}"/></option>
									</c:forEach>
								</select> <label for="kudos">Kudos <span class="usa-additional_text">Required</span></label>
								<textarea id="kudos" name="kudos" type="text" required="" aria-required="true" maxlength="250"></textarea>
								<button type="submit" name="go">Submit</button>
								<button type="reset">Reset</button>
							</fieldset>
						</form:form>
					</div>
					<button class="accordion">My Kudos</button>
					<div class="panel">
						<h2>My Kudos</h2>
						<c:if test="${not empty myKudos}">
							<div>
								<table>
									<thead>
										<tr>
											<th>Kudos</th>
											<th>By User</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="k" items="${myKudos}">
											<tr>
												<td>${k.kudos}</td>
												<td><c:out value="${k.byEmployee.firstName} ${k.byEmployee.lastName}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:if>
					</div>
					<button class="accordion">My Info</button>
					<div class="panel">
						<h2>My Info</h2>
						<table>
							<tr>
								<td><c:out value="${employeeObj.firstName} ${employeeObj.lastName}" /></td>
								<td><c:out value="${employeeObj.email}" /></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="usa-width-one-fourth"></div>
			</div>
		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
			acc[i].onclick = function() {
				this.classList.toggle("active");
				this.nextElementSibling.classList.toggle("show");
			}
		}
	</script>
</body>
</html>
