/*
 *
 */
package gov.dhs.kudos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.State;

@Transactional
public interface UserRepository extends JpaRepository<Employee, Long> {
	public Employee findByEmail(String email);

	public List<Employee> findByState(State state);

	public List<Employee> findByRole(Role role);

}