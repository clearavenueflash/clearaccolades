package gov.dhs.kudos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;

@Transactional
public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByType(RoleType type);

}
