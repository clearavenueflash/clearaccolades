package gov.dhs.kudos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.State;
import gov.dhs.kudos.model.forms.UserForm;

@Controller
public class AdminController extends BaseController {

	private static Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "admin/admin";
	}

	@RequestMapping(value = "/admin/employeeListing", method = RequestMethod.GET)
	public String employeeListing(final ModelMap modelMap) {
		modelMap.addAttribute("user", getPrincipal());
		modelMap.addAttribute("allUsers", userRepo.findAll());
		return "admin/employeeListing";
	}

	@RequestMapping(value = "/admin/pointsPage", method = RequestMethod.GET)
	public String pointsPage(final ModelMap modelMap){
		modelMap.addAttribute("user", getPrincipal());
		modelMap.addAttribute("allKudos", kudosRepo.findAll());
		return "admin/pointsPage";
	}

	@RequestMapping(value = "/admin/exportEmployeeListing", method = RequestMethod.GET)
	public void exportEmployeeListing(HttpServletResponse response) throws IOException {
		String csvFileName = "employeeListing.csv";
		response.setContentType("text/csv");

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);

		// uses the Super CSV API to generate CSV data from the model data
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "Last Name", "First Name", "Middle Name" };

		csvWriter.writeHeader(header);

		List<Employee> allUsers = userRepo.findAll();

		for (Employee u : allUsers) {
			csvWriter.write(u, header);
		}
		csvWriter.close();
	}

	@RequestMapping(value = "/admin/createUser", method = RequestMethod.GET)
	public String createUser(final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		model.addAttribute("allroles", roleRepo.findAll());
		return "admin/userForm";
	}

	@RequestMapping(value = "/admin/modifyUser", method = RequestMethod.GET)
	public String modifyUser(final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		model.addAttribute("allusers", userRepo.findAll());
		return "admin/userSelect";
	}

	@RequestMapping(value = "/admin/modifyUser/{id}", method = RequestMethod.GET)
	public String approve(@PathVariable final String id, final ModelMap model) {
		model.addAttribute("user", getPrincipal());
		model.addAttribute("allroles", roleRepo.findAll());

		final Employee modifyEmployee = userRepo.findOne(Long.parseLong(id));
		model.addAttribute("modifyEmployee", modifyEmployee);

		return "admin/userForm";
	}

	@RequestMapping(value = "/admin/processUser", method = RequestMethod.POST)
	public String registerUser(@Valid @ModelAttribute("userForm") final UserForm form, final BindingResult result,
			final ModelMap model) {

		if (result.hasErrors()) {
			model.addAttribute("errorMessage", result.getAllErrors().toString());
		} else {

			LOGGER.info("Got userform: {}", form);

			if (!StringUtils.equals(form.getPassword(), form.getConfirmPassword())) {
				model.addAttribute("errorMessage", "Passwords not the same");
			} else {

				final Employee employee = StringUtils.isNotBlank(form.getId())
						? userRepo.findOne(Long.valueOf(form.getId())) : new Employee();
				employee.setEmail(form.getEmail());
				employee.setFirstName(form.getFirstName());
				employee.setLastName(form.getLastName());
				employee.setState(State.APPROVED);

				if (StringUtils.isNotBlank(form.getRole())) {
					final Role role = roleRepo.findOne(Long.valueOf(form.getRole()));
					employee.setRole(role);
				} else {
					employee.setRole(roleRepo.findByType(RoleType.EMPLOYEE));
				}

				// new employee so add password
				if (employee.getId() == 0) {
					employee.setPassword(form.getPassword());
				}

				LOGGER.info("Creating employee: {}", employee);
				userRepo.save(employee);
			}
		}

		return "redirect:/admin";
	}

}
