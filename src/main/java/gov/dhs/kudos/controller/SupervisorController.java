package gov.dhs.kudos.controller;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Kudos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shane Hogan on 8/31/2016.
 */
@Controller
public class SupervisorController extends BaseController{

    private static Logger LOGGER = LoggerFactory.getLogger(SupervisorController.class);

    @RequestMapping(value = "/supervisor", method = RequestMethod.GET)
    public String index(final HttpServletRequest request, final ModelMap model) {
        model.addAttribute("employee", getPrincipal());
        Employee currentLoggedInUser = getLoggedInUser();
        if(currentLoggedInUser.getKudos() != null){
            model.addAttribute("myKudos", currentLoggedInUser.getKudos());
        }else{
            model.addAttribute("myKudos", new ArrayList<Kudos>());
        }
        model.addAttribute("myInfo", currentLoggedInUser);
        model.addAttribute("allUsers", userRepo.findAll());

        List<Kudos> communityKudos = kudosRepo.findAll();
        model.addAttribute("communityKudos", communityKudos);
        return "supervisor/SupervisorLanding";
    }

}
