package gov.dhs.kudos.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.State;

@Controller
public class SecurityController extends BaseController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(final Model model) {
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(final HttpServletRequest request, final HttpServletResponse response) {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	// TODO: Remove before production
	@RequestMapping(value = "/initData", method = RequestMethod.GET)
	public String initData() {

		kudosRepo.deleteAll();
		userRepo.deleteAll();
		roleRepo.deleteAll();

		// add roles
		final Role adminRole = roleRepo.save(new Role(RoleType.ADMIN));
		final Role userRole = roleRepo.save(new Role(RoleType.EMPLOYEE));

		// add employees
		final Employee admin = new Employee("Admin", "Admin", "password1", "admin@clearavenue.com", State.APPROVED);
		admin.setRole(adminRole);

		final Employee employee1 = new Employee("User", "One", "password1", "user.one@clearavenue.com", State.APPROVED);
		employee1.setRole(userRole);

		final Employee employee2 = new Employee("User", "Two", "password1", "user.two@clearavenue.com", State.APPROVED);
		employee2.setRole(userRole);

		final Employee employee3 = new Employee("User", "Three", "password1", "user.three@clearavenue.com", State.APPROVED);
		employee3.setRole(userRole);

		userRepo.save(Arrays.asList(employee1, employee2, employee3, admin));

		// add
		return "redirect:/";
	}
}
