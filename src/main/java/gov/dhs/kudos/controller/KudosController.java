package gov.dhs.kudos.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Kudos;
import gov.dhs.kudos.model.forms.KudosForm;

@Controller
public class KudosController extends BaseController {

	private static Logger LOGGER = LoggerFactory.getLogger(KudosController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(final HttpServletRequest request, final ModelMap model) {
		String page = "index"; // until we convert all pages such as admin landing

		final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		if (authorities.contains(new SimpleGrantedAuthority("ROLE_EMPLOYEE"))) {
			page = "redirect:/landing/employee";
		} else if (authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
			page = "redirect:/admin/pointsPage";
		}

		return page;
	}

	// TODO: Removed becuase no SUPERVISOR role defined now
	// @RequestMapping(value = "/supervisor/processKudos", method = RequestMethod.POST)
	// public String giveKudosFromSup(@Valid @ModelAttribute("kudosForm") final KudosForm form, final BindingResult result, final ModelMap model) {
	// final Employee forEmployee = userRepo.findOne(Long.valueOf(form.getForEmployee()));
	// final Employee byEmployee = getLoggedInUser();
	//
	// final Kudos kudos = new Kudos();
	// kudos.setByEmployee(byEmployee);
	// kudos.setForEmployee(forEmployee);
	// kudos.setKudos(form.getKudos());
	// kudosRepo.save(kudos);
	//
	// model.addAttribute("user", getPrincipal());
	//
	// return "redirect:/supervisor";
	// }

	@Transactional
	@RequestMapping(value = "/employee/processKudos", method = RequestMethod.POST)
	public String giveKudosFromEmpl(@Valid @ModelAttribute("kudosForm") final KudosForm form, final BindingResult result, final ModelMap model) {
		final Employee forEmployee = userRepo.findOne(Long.valueOf(form.getForEmployee()));
		final Employee byEmployee = getLoggedInUser();

		final Kudos kudos = new Kudos();
		kudos.setByEmployee(byEmployee);
		kudos.setForEmployee(forEmployee);
		kudos.setKudos(form.getKudos());

		kudosRepo.save(kudos);

		model.addAttribute("user", getPrincipal());

		return employeeLanding(model);
	}

	@RequestMapping(value = "/landing/employee", method = RequestMethod.GET)
	public String employeeLanding(final ModelMap model) {
		model.addAttribute("employee", getPrincipal());

		// my kudos list
		final Employee employee = getLoggedInUser();
		if (employee.getKudos() != null) {
			model.addAttribute("myKudos", employee.getKudos());

			// employee info
			model.addAttribute("employeeObj", employee);
		} else {
			model.addAttribute("myKudos", new ArrayList<Kudos>());
		}
		model.addAttribute("allusers", userRepo.findAll());
		return "employee/employeeLanding";
	}
}
