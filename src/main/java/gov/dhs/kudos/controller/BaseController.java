package gov.dhs.kudos.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.repository.KudosRepository;
import gov.dhs.kudos.repository.RoleRepository;
import gov.dhs.kudos.repository.UserRepository;

@Controller
public class BaseController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	RoleRepository roleRepo;

	@Autowired
	KudosRepository kudosRepo;

	protected String getPrincipal() {
		String userName = StringUtils.EMPTY;

		final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			final String email = ((UserDetails) principal).getUsername();
			final Employee employee = userRepo.findByEmail(email);
			userName = String.format("%s %s", employee.getFirstName(), employee.getLastName());
		} else {
			userName = principal.toString();
		}

		return userName;
	}

	protected Employee getLoggedInUser() {

		Employee employee = null;

		final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			final String email = ((UserDetails) principal).getUsername();
			employee = userRepo.findByEmail(email);
		}

		return employee;
	}

}
