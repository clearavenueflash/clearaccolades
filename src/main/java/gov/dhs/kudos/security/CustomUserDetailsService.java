package gov.dhs.kudos.security;

import java.util.ArrayList;
import java.util.List;

import gov.dhs.kudos.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.State;
import gov.dhs.kudos.repository.UserRepository;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
		final Employee employee = userRepo.findByEmail(email);

		if (employee == null) {
			throw new UsernameNotFoundException("Employee email not found");
		}

		final org.springframework.security.core.userdetails.User securityUser = new org.springframework.security.core.userdetails.User(employee.getEmail(), employee.getPassword(),
				employee.getState().equals(State.APPROVED), true, true, true, getGrantedAuthorities(employee));

		return securityUser;
	}

	private List<GrantedAuthority> getGrantedAuthorities(final Employee user) {
		final List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().getType()));
		return authorities;
	}

}