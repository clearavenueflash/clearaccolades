package gov.dhs.kudos;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@PropertySources({
	@PropertySource(value = "classpath:dhs-accolades.properties", ignoreResourceNotFound = true),
	@PropertySource(value = "file:${DHS_ACCOLADES_HOME}/dhs-accolades.properties", ignoreResourceNotFound = true)
})
@Configuration
@ComponentScan
public class SpringRootConfig {
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
