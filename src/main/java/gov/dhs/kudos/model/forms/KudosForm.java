package gov.dhs.kudos.model.forms;

public class KudosForm {

	private String kudos;
	private String forEmployee;

	public String getKudos() {
		return kudos;
	}

	public void setKudos(final String val) {
		kudos = val;
	}

	public String getForEmployee() {
		return forEmployee;
	}

	public void setForEmployee(final String val) {
		forEmployee = val;
	}

}
