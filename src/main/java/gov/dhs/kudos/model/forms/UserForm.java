package gov.dhs.kudos.model.forms;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserForm {

	private String email;
	private String password;
	private String confirmPassword;
	private String firstName;
	private String lastName;
	private String id;
	private String role;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String val) {
		email = val;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String val) {
		firstName = val;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String val) {
		lastName = val;
	}

	public String getId() {
		return id;
	}

	public void setId(final String val) {
		id = val;
	}

	public String getRole() {
		return role;
	}

	public void setRole(final String val) {
		role = val;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(final String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

}
