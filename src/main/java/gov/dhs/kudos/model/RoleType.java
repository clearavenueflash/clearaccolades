package gov.dhs.kudos.model;

public enum RoleType {
	EMPLOYEE("EMPLOYEE"),
	ADMIN("ADMIN");

	String roleType;

	private RoleType(final String val) {
		roleType = val;
	}

	public String getRoleType() {
		return roleType;
	}

}