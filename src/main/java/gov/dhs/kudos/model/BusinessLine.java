/*
 *
 */
package gov.dhs.kudos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "BUSINESSLINE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BusinessLine implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BUSINESSLINE_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String businessLine;

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getBusinessLine() {
		return businessLine;
	}

	public void setBusinessLine(final String programOffice) {
		businessLine = programOffice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessLine == null) ? 0 : businessLine.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final BusinessLine other = (BusinessLine) obj;
		if (businessLine == null) {
			if (other.businessLine != null) {
				return false;
			}
		} else if (!businessLine.equals(other.businessLine)) {
			return false;
		}
		return true;
	}

}