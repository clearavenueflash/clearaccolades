package gov.dhs.kudos.model;

import javax.annotation.PostConstruct;
import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
public class DatabaseListener {

	static private BCryptPasswordEncoder passwordEncoder;
	static final Logger LOGGER = LoggerFactory.getLogger(DatabaseListener.class);

	@Autowired(required = true)
	@Qualifier("passwordEncoder")
	@SuppressFBWarnings(value = "ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD", justification = "Doing this is correct to autowire in a EventListener")
	public void setPasswordEncoder(final BCryptPasswordEncoder passwordEncoder) {
		DatabaseListener.passwordEncoder = passwordEncoder;
	}

	@PostConstruct
	public void init() {
		LOGGER.debug("******* Initializing passwordEncoder for Listener [{}]", passwordEncoder);
	}

	@PrePersist
	public void hashPassword(final Object obj) {
		if (obj instanceof Employee) {
			hashPassword((Employee) obj);
		}
	}

	private void hashPassword(final Employee employee) {
		final String pass = employee.getPassword();
		final String encoded = passwordEncoder.encode(pass);
		employee.setPassword(encoded);
	}
}