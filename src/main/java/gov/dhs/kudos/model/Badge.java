package gov.dhs.kudos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Entity
@Table(name = "BADGE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Badge implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BADGE_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String badgeName;

	@Column(nullable = false)
	private int badgeLevel;

	@Lob
	private byte[] badgeImage;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + badgeLevel;
		result = prime * result + ((badgeName == null) ? 0 : badgeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Badge other = (Badge) obj;
		if (badgeLevel != other.badgeLevel) {
			return false;
		}
		if (badgeName == null) {
			if (other.badgeName != null) {
				return false;
			}
		} else if (!badgeName.equals(other.badgeName)) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "This is what we want the field called")
	public byte[] getBadgeImage() {
		return badgeImage;
	}

	@SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "This is what we want the field called")
	public void setBadgeImage(final byte[] badgeImage) {
		this.badgeImage = badgeImage;
	}

}