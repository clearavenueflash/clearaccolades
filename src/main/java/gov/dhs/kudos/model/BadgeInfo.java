package gov.dhs.kudos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Entity
@Table(name = "BADGEINFO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BadgeInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BADGEINFO_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private Badge badge;

	@ManyToOne
	private Employee forEmployee;

	public Badge getBadge() {
		return badge;
	}

	public void setBadge(final Badge badge) {
		this.badge = badge;
	}

	@SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "This is what we want the field called")
	public Date getBadgeReceived() {
		return badgeReceived;
	}

	@SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "This is what we want the field called")
	public void setBadgeReceived(final Date badgeReceived) {
		this.badgeReceived = badgeReceived;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date badgeReceived;

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Employee getForEmployee() {
		return forEmployee;
	}

	public void setForEmployee(final Employee forEmployee) {
		this.forEmployee = forEmployee;
	}

}