/*
 *
 */
package gov.dhs.kudos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "DIRECTORATE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Directorate implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DIRECTORATE_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String directorate;

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDirectorate() {
		return directorate;
	}

	public void setDirectorate(final String programOffice) {
		directorate = programOffice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((directorate == null) ? 0 : directorate.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Directorate other = (Directorate) obj;
		if (directorate == null) {
			if (other.directorate != null) {
				return false;
			}
		} else if (!directorate.equals(other.directorate)) {
			return false;
		}
		return true;
	}

}