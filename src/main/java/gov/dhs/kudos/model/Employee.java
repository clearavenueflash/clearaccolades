/*
 *
 */
package gov.dhs.kudos.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "employee_accounts")
@EntityListeners(DatabaseListener.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EMPLOYEE_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotEmpty
	@Column(name = "EMAIL", unique = true, nullable = false, length = 50)
	private String email;

	@NotEmpty
	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@NotEmpty
	@Column(name = "FIRSTNAME", nullable = false, length = 20)
	private String firstName;

	@NotEmpty
	@Column(name = "LASTNAME", nullable = false, length = 20)
	private String lastName;

	@Column(name = "MIDDLEINITIAL")
	private String middleInitial;

	@Enumerated(EnumType.STRING)
	@Column(name = "STATE", nullable = false)
	private State state = State.PENDING;

	@ManyToOne
	private ProgramOffice programOffice;

	@ManyToOne
	private Directorate directorate;

	@ManyToOne
	private BusinessLine businessLine;

	@ManyToOne
	private Division division;

	@ManyToOne
	private Role role;

	@OneToMany(mappedBy = "forEmployee", fetch = FetchType.EAGER)
	private List<Kudos> kudos;

	@OneToMany(mappedBy = "forEmployee", fetch = FetchType.EAGER)
	private List<BadgeInfo> badgeInfo;

	public Employee() {
	}

	public Employee(final String inFirstname, final String inLastname, final String inPwd, final String inEmail, final State inState) {
		firstName = inFirstname;
		lastName = inLastname;
		password = inPwd;
		email = inEmail;
		state = inState;
	}

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String val) {
		email = val;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String val) {
		password = val;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String val) {
		firstName = val;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String val) {
		lastName = val;
	}

	public State getState() {
		return state;
	}

	public void setState(final State val) {
		state = val;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(final Role val) {
		role = val;
	}

	public List<Kudos> getKudos() {
		return kudos;
	}

	public void setKudos(final List<Kudos> val) {
		kudos = val;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public ProgramOffice getProgramOffice() {
		return programOffice;
	}

	public void setProgramOffice(ProgramOffice programOffice) {
		this.programOffice = programOffice;
	}

	public Directorate getDirectorate() {
		return directorate;
	}

	public void setDirectorate(Directorate directorate) {
		this.directorate = directorate;
	}

	public BusinessLine getBusinessLine() {
		return businessLine;
	}

	public void setBusinessLine(BusinessLine businessLine) {
		this.businessLine = businessLine;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Employee other = (Employee) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		if (state != other.state) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<BadgeInfo> getBadgeInfo() {
		return badgeInfo;
	}

	public void setBadgeInfo(final List<BadgeInfo> badgeInfo) {
		this.badgeInfo = badgeInfo;
	}

}