/*
 * .To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 *  
 * This test suite can be run in netbeans IDE 8.1.
 *
 *
 */
package selenium;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Reporter;
/**
 *
 * @author Team clearAvenue
 */
public class Login {
    private static String baseUrl ="https://agile.clearavenue.com";
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    public Login() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
     public void loginInvalidCreds() {
         // TODO code application logic here
        // TODO code application logic here
        System.setProperty("webdriver.firefox.marionette","D:\\GeckoDriver\\geckodriver.exe");
        Reporter.log("Testing Invalid Credentials  no email and invalid password");
        //Here we initialize the firefox webdriver
        WebDriver firefoxDriver=new FirefoxDriver();       
        firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Open the url which we want in firefox
        //firefoxDriver.get("Http://www.google.com");
        firefoxDriver.get(baseUrl + "/clearaccolades/login");
        firefoxDriver.findElement(By.id("password")).clear();
        firefoxDriver.findElement(By.id("password")).sendKeys("password");
       
     }
    @Test
     public void loginGoodCredsAdmin() {
         // TODO code application logic here
        // TODO code application logic here
        System.setProperty("webdriver.firefox.marionette","D:\\GeckoDriver\\geckodriver.exe");
        //Here we initialize the firefox webdriver
        WebDriver firefoxDriver=new FirefoxDriver();        
        firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Open the url which we want in firefox
        //firefoxDriver.get("Http://www.google.com");
         Reporter.log("Testing Valid Credentials as admin");
        firefoxDriver.get(baseUrl + "/clearaccolades/login");
        firefoxDriver.findElement(By.id("password")).clear();
        firefoxDriver.findElement(By.id("password")).sendKeys("password1");
        firefoxDriver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
       
     }
     
     @Test
     public void loginGoodCredsUser() {
         // TODO code application logic here
        // TODO code application logic here
        System.setProperty("webdriver.firefox.marionette","D:\\GeckoDriver\\geckodriver.exe");
        //Here we initialize the firefox webdriver
        WebDriver firefoxDriver=new FirefoxDriver();       
        firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Open the url which we want in firefox
        //firefoxDriver.get("Http://www.google.com");
         Reporter.log("Testing Valid Credentials as Normal User");
        firefoxDriver.get(baseUrl + "/clearaccolades/login");
        firefoxDriver.findElement(By.id("password")).clear();
        firefoxDriver.findElement(By.id("email")).clear();
        firefoxDriver.findElement(By.id("email")).sendKeys("user.one@clearavenue.com");
        firefoxDriver.findElement(By.id("password")).sendKeys("password1");
        firefoxDriver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
       
     }
     
     @Test
     public void CreateuserAsAdmin() {
         // TODO code application logic here
        // TODO code application logic here
        System.setProperty("webdriver.firefox.marionette","D:\\GeckoDriver\\geckodriver.exe");
        //Here we initialize the firefox webdriver
        WebDriver firefoxDriver=new FirefoxDriver();     
        firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Open the url which we want in firefox
        //firefoxDriver.get("Http://www.google.com");
        Reporter.log("Testing CreateUser as Admin");
        firefoxDriver.get(baseUrl + "/clearaccolades/login");
        firefoxDriver.findElement(By.id("password")).clear();
        firefoxDriver.findElement(By.id("email")).clear();
        firefoxDriver.findElement(By.id("email")).sendKeys("admin@clearavenue.com");
        firefoxDriver.findElement(By.id("password")).sendKeys("password1");
        firefoxDriver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
        firefoxDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        firefoxDriver.findElement(By.id("createButton")).click();
     }
     
     @Test 
     public void deleteUser() {
          Reporter.log("Testing delete user as Admin");
     }
     @Test 
     public void addUser() {
          Reporter.log("Testing add User as Admin");
     }
     
     @Test 
     public void enableUser() {
          Reporter.log("Testing enable user");
     }
     @Test 
     public void disableleUser() {
          Reporter.log("Testing disable user");
     }
     @Test 
     public void addNewBadgeLevel() {
          Reporter.log("Testing add New Badge Level as Admin");
     }
     @Test 
     public void updateBadgeLevel() {
          Reporter.log("Testing update Badge Level as Admin");
     }
     @Test 
     public void deleteBadgeLevel() {
          Reporter.log("Testing delete Badge Level as Admin");
     }
     @Test 
     public void adminSearchComments() {
          Reporter.log("Testing search comments as Admin");
     }
     
     
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
